//To know when to use late vs. required

//Use required when logically you always require that variable to have a value.
//A person will always have a first name and last name.
//Then, use required for both first and last name.

//Use late when logically, a variable can have a null value.
//A person may or may not have a spouse
//Then use late for spouseIdfeild
//Use late if you are that later on your program, the value will be given to 


class Building {
    String? _name;
    int floors;
    String address;
//required is needed for optional
    Building(
      this._name,
      {
        required this.floors,
        required this.address
        }) {
    print ('a building object has been created');
  }
  //the get and set allows indirect access to class field
  //setter
  void set Name(String? name ){
    this._name = name;
print('The $name building name has been changed');
this._name = name;
  }
//getter
  String? get Name {
    print('the building is retrieved');
    return this._name;
  }
  Map <String, dynamic> getProperties(){
    return{
      'name' : this._name,
      'floors' : this.floors,
      'address' : this.address
    };
  }
}