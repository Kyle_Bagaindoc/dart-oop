class Building {
    String? _name;
    int floors;
    String address;
//required is needed for optional
    Building(
      this._name,
      {
        required this.floors,
        required this.address
        }) {
    print ('a building object has been created');
  }
  //the get and set allows indirect access to class field
  //setter
  void set Name(String? name ){
    this._name = name;
print('The $name building name has been changed');
this._name = name;
  }
//getter
  String? get Name {
    print('the building is retrieved');
    return this._name;
  }
  Map <String, dynamic> getProperties(){
    return{
      'name' : this._name,
      'floors' : this.floors,
      'address' : this.address
    };
  }
}