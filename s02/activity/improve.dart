void main(){

  List<num> prices = [45, 32, 176.9, 32.2];
    num totalPrice =  getTotalPrice(prices);

    prices.forEach((num price){
      totalPrice += price;
    });

    print(totalPrice);
    print(getDiscountedPrice(totalPrice, 0.2));
}

num getTotalPrice(List<num> prices) {

  num totalPrice = 0;

  prices.forEach((num price) {
    totalPrice += price;

  });
 return totalPrice;

}

num getDiscountedPrice(num totalPrice, num percentage){
return totalPrice - (totalPrice * percentage);

}
// Function getDiscount(num percentage){
//   //When the get discount is used and the function below is returned
//   //The value of 'percentage' parameter is retained
//   //That is called Lexical Scope.
//   return (num amount){
//     return amount * percentage /100;

//   };
// }


// num getTotalPrice(List<num> prices)=> prices.reduce {