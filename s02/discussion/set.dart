void main(){
  /*
  Set <datatype> variableName = {values};
  String firstName = 'John'; toString()
  String first_name = 'John';
 
  A set is an unordered collection of unique items
  List can be ordered
  //{'Sonderhoff', 'Stahlschimdt','Stahlschimdt'}; 
  //Both will still work except one will only appear
   */
  Set<String> subcontractors = {'Sonderhoff', 'Stahlschimdt'};
    subcontractors.add('Schweisstechnik');
    subcontractors.add('Kreatel');
    subcontractors.add('Kunstoffe');
    subcontractors.remove('Sonderhoff');
  print(subcontractors);
}