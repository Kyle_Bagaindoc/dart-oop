void main(){
  // List<dataType> variableName = [values];
List<int> discountRanges = [20, 40, 60, 80];
List<String> names =  [
    'John',
    'Jane',
    'Tom'
  ];

const List<String> maritalStatus = [
  'single',
  'married',
  'divorced',
  'widowed'
];
  print(discountRanges);
  print(names);

  print(discountRanges);
  print(names[0]);

  print(discountRanges.length);
  print(names.length);

  names[0] = 'Jonathan';
  print(names);

  
names.add('Mark'); //Add element to the end of the list
names.insert(0,'Roselle'); //Add element to the end of the list
print(names);
print(maritalStatus);

//properties
print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);

  //method
  //this modifies list itself
  // names.sort((a,b) => a.length.compareTo(b.length));
names.sort();
  print(names.reversed);
  }