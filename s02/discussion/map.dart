void main(){
  //Map <keyDataType, valueDataType> variableName = {values}
  Map <String,String> address = {
    'specifics': '221B Baker Street',
    'district': 'Marylebone',
    'city': 'London',
    'country': 'United States',
    'region': 'Europe'
  };

  address ['region'] = 'Europe';

  print(address);
}

//Key value pairs