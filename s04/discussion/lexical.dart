void main(){

Function discountBy25 = getDiscount(25);
Function discountBy50 = getDiscount(50);
//The discountBy25 od then considered as a closure.
//The closure has access to variables in it's lexical scope
  print(discountBy25(1400));
  print(discountBy50(1400));
}

Function getDiscount(num percentage){
  //When the get discount is used and the function below is returned
  //The value of 'percentage' parameter is retained
  //That is called Lexical Scope.
  return (num amount){
    return amount * percentage /100;
  };
}

//