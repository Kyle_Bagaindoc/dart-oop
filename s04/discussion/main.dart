
//The main function is the entry point of Dart applications.
// String companyName = getCompanyName();
void main() {
  
  print(getCompanyName());
  print(getYearEstablished());
  print(hasOnlineClasses());
  print(getCoordinates());
  print(combineAddress('134 Timog Avenue', 'Brgy. Sacred Heart', 'Quezon City', 'Metro Manila'));
  print(combineName('John', 'Smith'));
  print(combineName('John', 'Smith', isLastNameFirst: true));
  print(combineName('John', 'Smith', isLastNameFirst: false));


//Anonymous function = cases wherein you only use the code once
// List<String > persons = ['John Doe', 'Jane Doe'];
// List<String> students = ['Nicholas Rush', 'James Holden'];
// persons.forEach((String person) {
//   print (person);
  
// });

// students.forEach((String person) { 
//   print(person);
// });

// }

//Functions as objects AND used as an argument
//printName is reference to the given function
// persons.forEach((String person) {
//   print (person);
  
// });

// students.forEach((String person) { 
//   print(person);
// });



// void printName (String name){
//   print(name);

// }
//printName(value) is function execution/call/invocation
// persons.forEach(printName);
// students.forEach(printName);
}

/*
"Optional named parameters"
These are parameters added after the required ones.
These paratmeters are added inside a curly bracket.
if no value is given a default value can be assigned.
*/
String combineName(String firstName, String lastName,{bool isLastNameFirst = false}){
  if (isLastNameFirst){
    return '$lastName $firstName';
  }else{
  return '$firstName $lastName';
  }
}


String combineAddress (String specifics, String barangay, String city, String province){
  // return specifics + ' ' + barangay + ' ' + city + ' '+ province;
  return '$specifics, $barangay, $city, $province';
}

String getCompanyName(){
  return 'FFUF';
}

int getYearEstablished(){
  return 2021;
}

bool hasOnlineClasses() {
  return true;
}

// bool isUnderAge(int age){
//   return(age < 18)? true :false;
// }

/*
The initial isUnderAge function can be changed into a lambda (arrow) function.
A lambda function is a shortcut for returning values from simple operations
Syntax of a lanbda funtion is :
return-type function-name(parameters) => expression;
*/
bool isUnderAge (age) => (age< 18)? true : false;

Map<String, double> getCoordinates(){
  return{
    'latitude': 14.632,
    'longtitude':121.043
  };
}


//The following syntax is followed when creating a function
/*
returntype function-name(param-data-type parameterA, param-data-type parameterB){
  //code logic inside a function
  return 'return-value';
}
*/

//Arguments are the values being passed to the function.
//Parameters are the vaues being received by the function.
//x (parameter)= 5 (argument); 
//In dart, writing the data type of a function parameter is not required.


//The reason is related to code readability and conciseness.
//If no data type os specified to a parameter, it's default type is'dynamic'.