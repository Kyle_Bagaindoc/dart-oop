abstract class _Worker {
String getType() {
    return 'Worker' ;
}
}
class Carpenter implements _Worker{
    
    String getType(){
        return 'Carpenter';
    }  
}

//_ hindi dapat magamit parts ng code

/*
@override The override is an annotation.
  String _getType() {
    // TODO: implement _getType
    throw UnimplementedError();
  }
}
*/