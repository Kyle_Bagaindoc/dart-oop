import './worker.dart';

void main() {
    Doctor doctor = new Doctor(firstName: 'John', lastName: 'Smith');
    Carpenter carpenter = new Carpenter();
    print(doctor);
    print (carpenter.getType());

}

 abstract class Person {
     //The class person defines that a 'getfullName' must be implemented
     //However, it does not tell the concrete class HOW to implement it.
   
     String getFullName();

  }
//The concrete class is the Doctor Class.
  class Doctor implements Person {
        late String firstName;
        late String lastName;


//  Doctor( this.firstName,this.lastName);

    Doctor({
        required this.firstName,
        required this.lastName
    });
            String getFullName() {
            return 'Dr. ${this.firstName} ${this.lastName}';
            }
  }