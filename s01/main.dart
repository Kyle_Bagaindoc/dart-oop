//The keyword void means the function will not return anything.
//Syntax of a function is: return-type function-name(parameters){}


void main(){
  // console.log
  // System.out.println
  // print('Hello World');
  // var number = 1;
// name = number.toString();
String firstName = 'John'; 
// String? middleName = null;
String lastName = 'Smith';
int age = 31; //for numbers without the name variable
double height = 172.45; // for numbers with decimal points
num weight = 64.32;//can accept number with or without decimal points
bool isRegistered = false;
List<num> grades = [98.2,89,87,88,91.2]; //List is an array

// Map person = new Map(); Map is an object, one method is to create new map
// Map is an object
/* Map <String, String> 
 0: 'Brandon'
 1 : 213
 */

Map personA = { 
    'name': 'Brandon',
    'batch': 213 };


 //Alternative syntax for declaring object.
Map <String, dynamic> personB = new Map();
personB ['name'] = 'Juan';
personB ['batch'] = 89;

//With final once the value is set it cannot be changed
final DateTime now = DateTime.now();
// now  = DateTime.now(); It's fine if we don't give a value

//final DateTime now
// now  = DateTime.now(); It's fine if we don't give a value

//With const, an Identifier must have a corresponding declaration of value
// const String companyAcronym = 'FFUF';
// companyAcronym ='FFUF'; Has to have a value; can't be declared later



  // print(firstName + ' ' + lastName); //The hello is concatenated with the name variable
  print('Full name: $firstName $lastName');
  print ('Age :' + age.toString());
  print('Height: ' + height.toString());
  print ('Weight: ' + weight.toString());
  print('Registered' + isRegistered.toString());
  print('Grades' + grades.toString());
  print(personA);
  print(personB);
  print('Current Datetime' + now.toString());
  print(personB ['name']);

}
//TypeScript => Javascript 



