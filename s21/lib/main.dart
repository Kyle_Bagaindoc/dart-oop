import './models/user.dart';
import 'models/task.dart';

void main() {
    
    Task taskA = Task(id: 1, userId: 123, description: 'life',imageLocation: 'loc', isDone: 1);
    Task taskB = Task(id: 1, userId: 123, description: 'life', imageLocation:'loc', isDone: 1);


    bool isIdSame = taskA.id == taskB.id;
    bool isuserIdSame = taskA.userId == taskB.userId;
    bool isdescriptionSame = taskA.description == taskB.description;
    bool isimageLocationSame = taskA.imageLocation == taskB.imageLocation;
    bool isDoneSame = taskA.isDone == taskB.isDone;
    bool areObjectsSame = isIdSame && isuserIdSame && isdescriptionSame && isimageLocationSame && isDoneSame ;
    

      print(areObjectsSame);

    print(taskA.id);
    taskA.id = 1;
    print(taskA.id);
}

//Dart does not check the values of the object, but also its unique identifier(hashCode)