class Task{
    late final int id;
    late final int userId;
    late final String description;
    late final String? imageLocation;
    late final int isDone;



    Task({
        required this.id,
            required this.userId,
            required this.description,
            required this.imageLocation,
            required this.isDone,
    });
}
