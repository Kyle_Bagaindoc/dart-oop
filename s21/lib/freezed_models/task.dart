import 'package:freezed_annotation/freezed_annotation.dart';

part 'task.freezed.dart'; //freeze
part 'task.g.dart'; //Json 

@freezed
    class Task with _$Task {

        const factory Task({
            required this.id,
            required this.userId,
            required this.description,
            required this.imageLocation,
            required this.isDone,
        }) = _Task;

        factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);

    }