// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Task _$$_TaskFromJson(Map<String, dynamic> json) => _$_Task(
      id: json['id'],
      userId: json['userId'],
      description: json['description'],
      imageLocation: json['imageLocation'],
      isDone: json['isDone'],
    );

Map<String, dynamic> _$$_TaskToJson(_$_Task instance) => <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'description': instance.description,
      'imageLocation': instance.imageLocation,
      'isDone': instance.isDone,
    };
