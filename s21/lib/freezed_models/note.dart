import 'package:freezed_annotation/freezed_annotation.dart';

part 'note.freezed.dart'; //freeze
part 'note.g.dart'; //Json 

//You have a dart file that will have the rest of it's content in the user. freezed.dart
//Like you have different chapters in different files
//The rest of the contents for user.dart can be found in user.freezed.dart using the 'part' keyword.

// The@freezed class annotation tells the builder runner to create a freezed class named_$user in user.
//Makikita niya yung class na dapat nyang basahin to generate code automatically. Without this annotation, walang cide na magenerate:

@freezed
    class Note with _$Note {

        const factory Note({
             required this.id,
            required this.userId,
            required this.title,
            required this.description
        }) = _Note;

        factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);

    }