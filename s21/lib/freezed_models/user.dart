import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart'; //freeze
part 'user.g.dart'; //Json 

//You have a dart file that will have the rest of it's content in the user. freezed.dart
//Like you have different chapters in different files
//The rest of the contents for user.dart can be found in user.freezed.dart using the 'part' keyword.

// The@freezed class annotation tells the builder runner to create a freezed class named_$user in user.
//Makikita niya yung class na dapat nyang basahin to generate code automatically. Without this annotation, walang cide na magenerate:

@freezed
    class User with _$User {

        const factory User({
            required int id,
            required String email,
            String? accessToken
        }) = _User;

        factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

    }