import 'dart:html';

import './freezed_models/user.dart';

void main() {
    User userA = User(id: 1, email: 'john@gmail.com' );
    User userB = User(id: 1, email: 'john@gmail.com' );

   print(userA.hashCode);
   print(userB.hashCode);
   print(userA==userB);

//Demosntration of object immutability below.
//Immutability means changes are not allowed.
//It ensures that an object will not be changed accidentally

    // print(userA.email);
    // userA.email = 'john@hotmail.com';
    // print(userA.email);

//Instead of directly changing the object;s property, the object itself will be changed or re-assigned with new values.
//To achive this, we use the object.copyWith() method

    // print(userA.email);
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    var response = {'id': 3, 'email': 'doe@gmail.com'};
    //     User userC = User(id: response['id'] as int,
    //     email: response['email'] as String);
 
    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());

}

