void main(){
// whileLoop();
// doWhileLoop();
forLoop();
modefiedforLoop();
}
void whileLoop(){
  int count = 5;

  while (count != 0){
    print(count);

    count--;
  }

}
void doWhileLoop(){

  int count = 20;

  do {
    print (count);
    count--;
  } while (count > 0);

}

void forLoop(){
  // for (initializer, condition, iterator)
for (int count = 0; count < 20; count ++)
{
  print(count);
}

}

voidforInLoop(){
  List persons = [
  {'name': 'Brandon'},
  {'name': 'John'},
  {'name': 'Arthur'}
  ];


for (var person in persons){

  print(person['name']);
}
}
void modifiedforLoop() {
  for (int count = 0; count <= 20; count++) {
    if (count % 2 == 0) {
      continue;
    } else {
      print(count);
    }
    if (count > 10) {
      break;
    }
  }
}
