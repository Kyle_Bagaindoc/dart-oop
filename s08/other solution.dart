
void main() {

    Bulldozer bulldozer = new Bulldozer(
      name: 'Caterpillar D10',
      blade: 'U blade');

  TowerCrane towerCrane = new TowerCrane (
        name: '370 EC-B 12 Fibre',
        hookRadius: '78m hook radius', 
        capacity:'12,000kg');  

        Loader loader = new Loader( 
         name: 'Volvo L60H', 
        type : 'wheel loader', 
        tippingLoad :'16530 lbs');

   List<dynamic> equipment =[];
   equipment.add(bulldozer);
   equipment.add(towerCrane);
   equipment.add(loader);

   for (var i = 0; i < equipment.length; i++){
       print( equipment[i].describe());
   }
}

abstract class Equipment{
 String Describe();


}

class Bulldozer implements Equipment {
  late String name;
  late String blade;


  Bulldozer ({
  required this.name,
  required this.blade

});
    String Describe(){
        return 'The bulldozer ${this.name} has ${this.blade}.';

}

}

class TowerCrane implements Equipment {
    late String name;
    late String hookRadius;
    late String capacity;


    TowerCrane ({
  required this.name,
  required this.hookRadius,
  required this.capacity

});
    String Describe(){
        return 'The tower crane ${this.name} has a radius of ${this.hookRadius} and a max capacity of ${this.capacity}';

}
}

class Loader implements Equipment {
    late String name;
    late String type;
    late String tippingLoad;

Loader ({
  required this.name,
  required this.type,
  required this.tippingLoad

});

String Describe(){
        return 'The loader ${this.name} is a ${this.type} and has a tipping load of ${this.tippingLoad}';

}

}




