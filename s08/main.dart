
void main() {

   List<Equipment> equipmentType =[];
   
     equipmentType.add(Bulldozer(name: 'Caterpillar D10',
      blade: 'U blade'
      ));
     equipmentType.add(TowerCrane(
        name: '370 EC-B 12 Fibre',
        hookRadius: '78m hook radius', 
        capacity:'12,000kg'));

        equipmentType.add(Loader(
        name: 'Volvo L60H', 
        type : 'wheel loader', 
        tippingLoad :'16530 lbs'
    ));

    equipmentType.forEach((Bulldozer) => print(Bulldozer.Describe()));
  }

abstract class Equipment{
 String Describe();


}


class Bulldozer implements Equipment {
  late String name;
  late String blade;


  Bulldozer ({
  required this.name,
  required this.blade

});
    String Describe(){
        return 'The bulldozer ${this.name} has ${this.blade}.';

}

}

class TowerCrane implements Equipment {
    late String name;
    late String hookRadius;
    late String capacity;


    TowerCrane ({
  required this.name,
  required this.hookRadius,
  required this.capacity

});
    String Describe(){
        return 'The tower crane ${this.name} has a radius of ${this.hookRadius} and a max capacity of ${this.capacity}';

}
}

class Loader implements Equipment {
    late String name;
    late String type;
    late String tippingLoad;

Loader ({
  required this.name,
  required this.type,
  required this.tippingLoad

});

String Describe(){
        return 'The loader ${this.name} is a ${this.type} and has a tipping load of ${this.tippingLoad}';

}

}




